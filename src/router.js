import Vue from "vue";
import Router from "vue-router";
// import { getRole } from "./services/auth";

import Home from "./views/public/Home.vue";

import Layout from "./views/private/Layout.vue";

/* Professor routes */
import ProfessoresHomepage from "./views/private/Professores/Homepage.vue";
import Professores from "./views/private/Professores/Professores/Professores.vue";
import ProfessoresView from "./views/private/Professores/Professores/ProfessoresView.vue";
import ProfessoresCategorias from "./views/private/Professores/Categorias/Categorias.vue";
import ProfessoresCategoriasForm from "./views/private/Professores/Categorias/CategoriasForm.vue";
import ProfessoresCategoriasEdit from "./views/private/Professores/Categorias/CategoriasEdit.vue";
import ProfessoresPerfil from "./views/private/Professores/Perfil.vue";
import ProfessoresMateriais from "./views/private/Professores/Materiais/Materiais.vue";
import ProfessoresMateriaisEdit from "./views/private/Professores/Materiais/MateriaisEdit.vue";
import ProfessoresMateriaisForm from "./views/private/Professores/Materiais/MateriaisForm.vue";
import ProfessoresMateriaisSearch from "./views/private/Professores/Materiais/MateriaisSearch.vue";
import ProfessoresMateriaisView from "./views/private/Professores/Materiais/MateriaisView.vue";
import ProfessoresMateriaisFavoritos from "./views/private/Professores/Materiais/MateriaisFavoritos.vue";
import ProfessoresMateriaisEGrupos from "./views/private/Professores/Materiais/MateriaisEGrupos.vue";
import ProfessoresMateriaisDoGrupo from "./views/private/Professores/Materiais/MateriaisDoGrupo.vue";
import ProfessoresGrupoEdit from "./views/private/Professores/Materiais/GrupoEdit.vue";
import ProfessoresGrupoForm from "./views/private/Professores/Materiais/GrupoForm.vue";


/* Instituicao routes */
import InstituicaoHomepage from "./views/private/Instituicoes/Homepage.vue";
import NovoProfessor from "./views/private/Instituicoes/Professores/NovoProfessor.vue";
import InstituicaoProfessores from "./views/private/Instituicoes/Professores/Professores.vue";


Vue.use(Router);

const router = new Router ({
    linkExactActiveClass: "active",
    mode: "history",
    routes: [
        { 
            path: "/",
            redirect: "home", 
            component: Home
        },
        { 
            path: "/home",
            component: Home
        },
        { 
            path: "/instituicao",
            name: "instituição",
            component: Layout,
            children: [
                {
                    path: "/home",
                    name: "institute-homepage",
                    component: InstituicaoHomepage
                },
                {
                    path: "professores",
                    name: "professores",
                    component: InstituicaoProfessores
                },
                {
                    path: "professores/novo",
                    name: "novo professor",
                    component: NovoProfessor
                }
            ]
        },
        { 
            path: "/professor",
            redirect: "/professor/home",
            name: "professor",
            component: Layout,
            children: [
                {
                    path: "home",
                    name: "Início",
                    component: ProfessoresHomepage
                },
                {
                    path: "professores",
                    name: "Professores da Instituição",
                    component: Professores
                },
                {
                    path: "professores/:id",
                    name: "Ver Informações do Professor",
                    component: ProfessoresView
                },
                {
                    path: "categorias",
                    name: "Ver categorias",
                    component: ProfessoresCategorias
                },
                {
                    path: "categorias/nova",
                    name: "Adicionar nova categoria",
                    component: ProfessoresCategoriasForm
                },
                {
                    path: "categorias/:id",
                    name: "Ver e editar categoria",
                    component: ProfessoresCategoriasEdit
                },
                {
                    path: "perfil",
                    name: "Editar perfil",
                    component: ProfessoresPerfil
                },
                {
                    path: "materiais",
                    name: "Materiais",
                    component: ProfessoresMateriais
                },
                {
                    path: "materiais/buscar",
                    name: "Buscar materiais",
                    component: ProfessoresMateriaisSearch
                },
                {
                    path: "materiais/grupos/:id/novo",
                    name: "Adicionar novo material",
                    component: ProfessoresMateriaisForm
                },
                {
                    path: "materiais/:id/editar",
                    name: "Editar material",
                    component: ProfessoresMateriaisEdit
                },
                {
                    path: "materiais/:id/visualizar",
                    name: "Visualizar material",
                    component: ProfessoresMateriaisView
                },
                {
                    path: "materiais/favoritos",
                    name: "Visualizar favoritos",
                    component: ProfessoresMateriaisFavoritos
                },
                {
                    path: "materiais/pessoal",
                    name: "Visualizar meus grupos e materiais",
                    component: ProfessoresMateriaisEGrupos
                },
                {
                    path: "materiais/grupos/:id/materiais",
                    name: "Visualizar materiais do grupo",
                    component: ProfessoresMateriaisDoGrupo
                },
                {
                    path: "grupos/novo",
                    name: "Adicionar novo grupo de material",
                    component: ProfessoresGrupoForm
                },
                {
                    path: "grupos/:id/editar",
                    name: "Editar grupo de material",
                    component: ProfessoresGrupoEdit
                }
            ]
        },
        {
            path: "*",
            redirect: "home", 
            component: Home
        }
    ]
});
/* router.beforeEach((to, from, next) => {
    // if (getRole("admin")) return { path: "/instituicao/home" }
    // else 
    if (getRole("institute")) return { path: "/instituicao/home" }
    else if (getRole("teacher")) return { path: "/professor/home" }
    else next();
    
}) */
export default router;